import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormGroup , FormControl, FormBuilder} from '@angular/forms';
 import { ActivatedRoute, Router, ParamMap } from '@angular/router';
 import { Servis2018Service } from "../../servis2018.service";

@Component({
  selector: 'edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
@Output() updateUser:EventEmitter<any> = new EventEmitter<any>(); 
 @Output() updateUserPs:EventEmitter<any> = new EventEmitter<any>();
  //Q4 fix
  name;
  price;

  service:Servis2018Service;
  user;
  updateform = new FormGroup({
    name:new FormControl(),
    price:new FormControl()
  });

  //Q4 fix router
  constructor(private route: ActivatedRoute ,service: Servis2018Service, private formBuilder: FormBuilder, private router: Router) {
    this.service = service;
  }

  sendData() {
    this.updateUser.emit(this.updateform.value.name);
    console.log(this.updateform.value);

    this.route.paramMap.subscribe(params=>{

 let id = params.get('id');
      this.service.putUser(this.updateform.value, id).subscribe(
        response => {
          console.log(response.json());
          this.updateUserPs.emit();
          //Q4 fix
          this.router.navigate(['/']);
        }
      );
    })
  }
  
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.name);
        //this.name = this.user.name;
        //this.price = this.user.price  
      })
    })
  }
}


  