import { Component, OnInit } from '@angular/core';
import { Servis2018Service } from "../servis2018.service";

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
products;
productsKey;

 constructor(private service:Servis2018Service) {
    service.getUsers().subscribe(
      response=>{
        this.products = response.json();
        this.productsKey = Object.keys(this.products);
        console.log(response);
    });
  }


  ngOnInit() {
  }

}
