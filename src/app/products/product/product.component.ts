import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Servis2018Service } from "../../servis2018.service";
@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
user;

  constructor(private route: ActivatedRoute , private service:Servis2018Service) { }

  //קריאה של יוזר בודד
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);
      })
    })
  }
}

  

