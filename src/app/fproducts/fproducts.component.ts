import { Component, OnInit } from '@angular/core';
import { Servis2018Service } from "../servis2018.service";

@Component({
  selector: 'fproducts',
  templateUrl: './fproducts.component.html',
  styleUrls: ['./fproducts.component.css']
})
export class FproductsComponent implements OnInit {

users;

  constructor(private service:Servis2018Service) { }

  ngOnInit() {
    this.service.getUsersFire().subscribe(response=>{
        console.log(response);
        this.users = response;
    });
  }

}


