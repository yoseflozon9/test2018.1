import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersComponent } from './users/users.component';
import { Servis2018Service } from "./servis2018.service";
import { ProductsComponent } from './products/products.component';
import { UpdateFormComponent } from './update-form/update-form.component';
import { EditProductComponent } from "./products/edit-product/edit-product.component";
import { ProductComponent } from "./products/product/product.component";
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { environment } from "../environments/environment";
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FproductsComponent } from './fproducts/fproducts.component';
import { SearchResultsComponent } from './search-results/search-results.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    NavigationComponent,
    UsersComponent,
    ProductsComponent,
    UpdateFormComponent,
    EditProductComponent,
    ProductComponent,
    FproductsComponent,
    SearchResultsComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,

    RouterModule.forRoot([
      {path: '', component:ProductsComponent},
      {path: 'edit-product/:id', component:EditProductComponent},
      {path: 'product/:id', component:ProductComponent},
       {path: 'login', component:LoginComponent}, 
        {path: 'fproducts', component:FproductsComponent}, 
      {path: '**', component: NotFoundComponent}//להשאיר בכל מקרה
    ])

  ],
  providers:  [Servis2018Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
