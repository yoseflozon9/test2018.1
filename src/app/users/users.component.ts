import { Component, OnInit } from '@angular/core';
import { Servis2018Service } from "../servis2018.service";

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
users;
usersKeys;

 constructor(private service:Servis2018Service) {
    service.getUsers().subscribe(
      response=>{
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);
    });
  }


  ngOnInit() {
  }

}
