import { TestBed, inject } from '@angular/core/testing';

import { Servis2018Service } from './servis2018.service';

describe('Servis2018Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Servis2018Service]
    });
  });

  it('should be created', inject([Servis2018Service], (service: Servis2018Service) => {
    expect(service).toBeTruthy();
  }));
});
