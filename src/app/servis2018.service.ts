import { Injectable } from '@angular/core';
import { environment } from "../environments/environment";
 import { HttpParams } from '@angular/common/http';
 import { Http , Headers } from '@angular/http';
 import { AngularFireDatabase } from 'angularfire2/database';



@Injectable()
export class Servis2018Service {
  dbl: any;
  http: Http;

 constructor(http:Http, private db1:AngularFireDatabase) {
    this.http = http;
   }

getUsers(){
 return this.http.get(environment.url +'users');
}
getUser(id){
  return this.http.get(environment.url+ 'users'+'/'+'id');
}
getUsersFire(){
    //הוויליו מייצר את האובזווריבל
    return this.db1.list('/product').valueChanges();
  }

putUser(data,key){
 let options = {
   headers: new Headers({
     'content-type':'application/x-www-form-urlencoded'
   })
 }
 //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
 let params = new HttpParams().append('name',data.name).append('product',data.product);
 return this.http.put(environment.url + 'users/'+ key,params.toString(), options);

}}
