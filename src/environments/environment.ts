// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/angular/test/slim/',
  firebase:{
    apiKey: "AIzaSyCKUdvB6KNeWK1YCcnv7QUe-6hadU96wds",
    authDomain: "product-ea513.firebaseapp.com",
    databaseURL: "https://product-ea513.firebaseio.com",
    projectId: "product-ea513",
    storageBucket: "product-ea513.appspot.com",
    messagingSenderId: "321848587902"}
};
